# 🚀 Configuration Eslint et Prettier basé sur Airbnb.

## 🚔 Eslint

Ajouter le plugin Eslint en dev dependency en executant la commande suivante :

```shell
yarn add eslint -D
```

Créer la configuration d'Eslint en executant la commande suivante :

```shell
./node_modules/eslint/bin/eslint.js --init
```
- Choisir "To check syntax, find problems, and enforce code style"
- Choisir Javascript modules ou Commonjs au choix
- Choisir un framework ou non
- Choisir browser ou node selon le projet
- Et choisir un style populaire : Celui d'Airbnb
- Choisir le format .json 
- Confirmer l'installation des dépendances nécessaires

Ouvrir le fichier .eslintrc.json et y copier le contenu du fichier de ce repo (Ajuster selon le projet react ou non)

## 🎨 Prettier

Ajouter le plugin prettier en dev dependency en executant la commande suivante :

```shell
yarn add prettier -D
```

Créer un fichier .prettierrc à la racine du projet et y copier le contenu du fichier de ce repo.

### 🔥 Compatibilité Eslint & Prettier

Afin de faire en sorte que Prettier et Eslint ne rentrent pas en conflit lors du formatage, ajouter les dépendances suivantes :

```shell
yarn add -D eslint-config-prettier eslint-plugin-prettier
```

La configuration du fichier .eslintrc.json fait le reste 😉

### 🤘🏻 Bonus Vscode

- Ajouter le plugin Prettier afin de le choisir comme formateur par défaut.
  
- Ajouter le plugin Eslint afin de disposer de commandes de fix, listing des erreurs...

Ajouter les lignes suivantes dans le fichiers settings.json de Vscode afin de formater avec Prettier et résoudre les erreurs avec Eslint à chaque sauvegarde.

```json
  "editor.formatOnSave": true,
  "[javascrip]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.codeActionsOnSave": {
      "source.organizeImports": false,
      "source.fixAll": true
    }
  },
```
